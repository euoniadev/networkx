﻿using System;
using System.Globalization;
using System.Web;
using System.Windows.Data;

namespace NetworkX.Converters
{
	public class UrlEncodingConverter : IValueConverter
	{
		public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
		{
			return HttpUtility.UrlEncode(value as string);
		}

		public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
		{
			return HttpUtility.UrlDecode(value as string);
		}
	}
}
