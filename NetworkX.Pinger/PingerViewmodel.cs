﻿using NetworkX.Core.Command;
using NetworkX.Core.Contract;
using NetworkX.Core.Viewmodel;
using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;
using System.Text;
using System.Net.NetworkInformation;
using System.Windows.Input;

namespace NetworkX.Pinger
{
	[Export(typeof(IAddin))]
	public class PingerViewmodel : ViewmodelBase, IAddin
	{
		#region Constructor
		private Ping pingSender = new Ping();
		public PingerViewmodel()
		{
			pingSender.PingCompleted += PingCompleted;
		}

		private void PingCompleted(object sender, PingCompletedEventArgs e)
		{
			if (e.Error == null)
			{
				PingReply = e.Reply;
			}
			else
			{
				HasError = true;
				ErrorMessage = e.Error.Message;
			}
			IsBusy = false;
		}
		#endregion

		#region Interface
		public string Icon
		{
			get
			{
				return string.Empty;
			}
		}

		public string Name
		{
			get
			{
				return "Pinger";
			}
		}

		public IEnumerable<Submenu> Submenus
		{
			get
			{
				yield return new Submenu("home", "pack://application:,,,/NetworkX.Pinger;Component/Views/PingerView.xaml", true);
            }
		}
		#endregion

		#region Reply informations
		private PingReply _pingReply;
		public PingReply PingReply
		{
			get{ return _pingReply; }
			set
			{ 
				_pingReply = value;
				RaisePropertyChanged (() => PingReply);
			}
		}
		#endregion

		#region Properties
		private string _ipaddressOrHost;
		public string IPAddressOrHost
		{
			get { return _ipaddressOrHost; }
			set
			{
				_ipaddressOrHost = value;
				RaisePropertyChanged(() => IPAddressOrHost);
				RaisePropertyChanged(() => PingCommand);
			}
		}

		private bool _isbusy = false;
		public bool IsBusy
		{
			get
			{
				return _isbusy;
			}
			set
			{
				_isbusy = value;
				RaisePropertyChanged(() => IsBusy);
				RaisePropertyChanged(() => PingCommand);
			}
		}

		private bool _hasError = false;
		public bool HasError
		{
			get
			{
				return _hasError;
			}
			set
			{
				_hasError = value;
				RaisePropertyChanged(() => HasError);
			}
		}

		private string _errorMessage;
		public string ErrorMessage
		{
			get
			{
				return _errorMessage;
			}
			set {
				_errorMessage = value;
				RaisePropertyChanged(() => ErrorMessage);
			}
		}
		#endregion

		#region Command
		public ICommand PingCommand
		{
			get
			{
				return new GenericCommand() {
					CanExecuteCallback=delegate { return !string.IsNullOrEmpty(IPAddressOrHost) && !IsBusy; },
					ExecuteCallback=delegate
					{
						HasError = false;
						IsBusy = true;
						pingSender.SendAsync(IPAddressOrHost, 3000, null);
					}
				};
			}
		}
		#endregion
	}
}
