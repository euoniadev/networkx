﻿using NetworkX.Core.Contract;
using NetworkX.Core.Viewmodel;
using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;
using System.Text;

namespace NetworkX.Viewmodel
{
	[Export(typeof(IAddin))]
	public class HomeViewmodel : ViewmodelBase, IAddin
	{
		public string Icon
		{
			get
			{
				return string.Empty;
			}
		}

		public string Name
		{
			get
			{
				return "HOME";
			}
		}

		public IEnumerable<Submenu> Submenus
		{
			get
			{
				yield return new Submenu("Home", "pack://application:,,,/NetworkX;Component/Views/HomeView.xaml", true);
            }
		}
	}
}
