﻿using NetworkX.Core.Contract;
using NetworkX.Core.Viewmodel;
using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.ComponentModel.Composition.Hosting;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;

namespace NetworkX.Viewmodel
{
	public class FrameViewmodel : ViewmodelBase
	{
		public FrameViewmodel()
		{
			ComposeAssembly();
		}

		#region Addins
		[ImportMany(typeof(IAddin), AllowRecomposition = true)]
		public List<Lazy<IAddin>> ContractAddins { get; set; }

		private Lazy<IAddin> _selectedAddin;
		public Lazy<IAddin> SelectedAddin
		{
			get { return _selectedAddin; }
			set
			{
				_selectedAddin = value;
				RaisePropertyChanged(() => SelectedAddin);
				RaisePropertyChanged(() => ViewUri);
			}
		}
		#endregion

		#region Views
		public string ViewUri
		{
			get
			{
				if (SelectedAddin == null)
				{
					return "/Views/HomeView.xaml";
				}
				return SelectedAddin.Value.Submenus.FirstOrDefault().ViewUri;
			}
		}
		#endregion

		private bool ComposeAssembly()
		{
			string addins = System.Windows.Forms.Application.StartupPath;

			DirectoryCatalog addins_direc = new DirectoryCatalog(addins, "*.dll");

			var catalog = new AggregateCatalog();
			catalog.Catalogs.Add(new AssemblyCatalog(Assembly.GetExecutingAssembly()));
			catalog.Catalogs.Add(addins_direc);
			var provider = new ComposablePartExportProvider();

			var container = new CompositionContainer(catalog, provider);
			provider.SourceProvider = container;
			var batch = new CompositionBatch();
			batch.AddPart(this);

			try
			{
				container.Compose(batch);
			}
			catch (CompositionException exception)
			{
				Console.WriteLine(exception.ToString());
				return false;
			}
			return true;
		}
	}
}
