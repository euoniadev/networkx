﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;

namespace NetworkX.Core.Viewmodel
{
    public class ViewmodelBase:BaseClass
    {
		protected virtual GlobalViewmodel GlobalViewmodel
		{
			get {
				return Application.Current.FindResource("GlobalViewmodel") as GlobalViewmodel;
            }
		}
    }
}
