﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net.NetworkInformation;

namespace NetworkX.Core.Viewmodel
{
    public class GlobalViewmodel : BaseClass
    {
		public GlobalViewmodel()
		{
			NetworkChange.NetworkAvailabilityChanged -= NetworkAvailabilityChanged;
			NetworkChange.NetworkAvailabilityChanged += NetworkAvailabilityChanged;
		}

		private void NetworkAvailabilityChanged(object sender, NetworkAvailabilityEventArgs e)
		{
			RaisePropertyChanged(() => IsNetworkAvailable);
		}

		#region InternetIPAddress
		private string _internetIPAddress;
        public string InternetIPAddress
        {
            get { return _internetIPAddress; }
            set
            {
                _internetIPAddress = value;
                RaisePropertyChanged(() => InternetIPAddress);
            }
        }
        #endregion

        #region PhysicalAddress
        
        public IEnumerable<string> PhysicalAddress
        {
            get
            {
                return NetworkInterface.GetAllNetworkInterfaces().Select(t => t.GetPhysicalAddress().ToString());
            }
        }
        #endregion

        #region IsNetworkAvailable
        public bool IsNetworkAvailable
        {
            get
            {
                return NetworkInterface.GetIsNetworkAvailable();
            }
        }
        #endregion
    }
}
