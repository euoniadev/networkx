﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NetworkX.Core.Contract
{
    public interface IAddin
    {
        string Name { get; }

        IEnumerable<Submenu> Submenus { get; }

        string Icon { get; }
    }
}
