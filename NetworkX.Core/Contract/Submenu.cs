﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;

namespace NetworkX.Core.Contract
{
	public class Submenu
	{
		public Submenu() { }

		public Submenu(string title, string viewUri, bool isdefault = false)
		{
			Title = title;
			ViewUri = viewUri;
			IsDefault = isdefault;
		}

		public string Title { get; set; }

		public string ViewUri { get; set; }

		[DefaultValue(false)]
		public bool IsDefault { get; set; }
	}
}
