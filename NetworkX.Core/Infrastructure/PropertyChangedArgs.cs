﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;

namespace NetworkX.Core.Infrastructure
{
    public class PropertyChangedArgs : EventArgs
    {
        public PropertyInfo Property { get; set; }

        public object Value { get; set; }
    }
}
